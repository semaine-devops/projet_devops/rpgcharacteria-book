const express = require('express');
const app = express();
const admin = require('firebase-admin');
require('dotenv').config();

const bodyParser = require("body-parser");
const {set} = require("express/lib/application");
const res = require("express/lib/response");
const {query} = require("express");

// Content-type: application/json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

async function getCharactersList (userId) {
    let charactersList = [];
    const db = admin.firestore();
    const docRef = db.collection('book');
    await docRef.where("userId", "==", userId)
        .get()
        .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                charactersList.push(doc.data());
                //console.log(doc.id, " => ", doc.data());
            });
            //return charactersList;
        })
        .catch((error) => {
            console.log("Error getting documents: ", error);
            return [];
        });
    return charactersList;
}

async function pushbook(userID, email, image, name, age, gender, characterClass, characterrace, history, userId) {
    console.log("pushbook function ");

    function setdata(userID, image, name, age, gender, characterClass, characterrace, history, userId){
        console.log("setdata");
        const db = admin.firestore();
        const docRef = db.collection('book').doc();
        docRef.set({
            image: image,
            name: name,
            age: age,
            gender: gender,
            characterClass: characterClass,
            characterrace: characterrace,
            history: history,
            userId: userId,
        });
    }
    setdata(userID, email, image, name, age, gender, characterClass, characterrace, history, userId);
}

module.exports = {pushbook: pushbook, getCharactersList: getCharactersList};
