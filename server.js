const express = require('express');
const app = express();
const admin = require('firebase-admin');
require('dotenv').config();
const serviceAccount = require(process.env.CREDENCIALESLOG);
const jwt = require('jsonwebtoken');
var cors = require('cors')

// Content-type: application/json
app.use(cors())
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
})

const bodyParser = require("body-parser");
const {auth} = require("firebase-admin");
const {getAuth} = require("firebase-admin/auth");
const {pushbook, getCharactersList} = require("./bookhistory");

// Content-type: application/json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

app.post("/getbook", async (req, res) => {
    const jwtToken = req.body.jwtToken;
    console.log('TOKEN',jwtToken)
    const verify = await jwt.verify(jwtToken, process.env.SECRET)
    if (!verify) {
        res.status(403).json({
            error: {
                message: "Unauthorized",
            }
        });
        return;
    }
    const userId = verify.userId
    const charactersList = await getCharactersList(userId);
    res.status(200).json({
        charactersList: charactersList,
    });
});

app.post("/postbook", async (req, res) => {
    const jwtToken = await req.body.jwtToken;
    console.log(jwtToken);
    const jwtString = jwtToken.toString()
    const verify = await jwt.verify(jwtString, process.env.SECRET)
    if (!verify) {
        res.status(403).json({
            error: {
                message: "Unauthorized",
            }
        });
        return;
    }
    const userID = verify.userId

    product = {
        image: req.body.image,
        name: req.body.name,
        age: req.body.age,
        gender: req.body.gender,
        characterClass: req.body.characterClass,
        characterrace: req.body.characterrace,
        history: req.body.history,
        userID: userID
    }

    //recuperer le token de l'utilisateur
    if (userID !== undefined) {
        console.log("if before pushbook ")
        await pushbook(userID, product.image, product.name, product.age, product.gender, product.characterClass, product.characterrace, product.history, product.userID);
        res.json("book pushed");
    } else {
        res.json("mot de passe incorrect");
    }
});

const port = process.env.PORT;
app.listen(port, () => {
    console.log("Serveur démarré");
});
